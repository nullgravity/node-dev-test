import { Injectable, Inject, NotImplementedException } from '@nestjs/common';
import { Repository } from './Repository';
import { Product } from './../Model/Product';
import { Customer } from '../Model/Customer';

@Injectable()
export class OrderMapper {
  @Inject() repository: Repository;

  getBestSellers(date: Date): Promise<Product[]> {
    return Promise.all([
      this.repository.fetchOrdersForDay(date),
      this.repository.fetchProducts(),
    ])
      .then( results => {
        const [orders, allProducts] = results;

        const productsIds: any[] = orders.map( o => o.products ); // first find all
        const ids = [].concat.apply([], productsIds ); // flatten array

        // result is list of products, it is not aggregated yet
        return ids.map( id => allProducts.find( p => p.id === id ));
      });
  }

  getBestBuyers(date: Date): Promise<any[]> {
    return Promise.all([
      this.repository.fetchOrdersForDay(date),
      this.repository.fetchCustomers(),
      this.repository.fetchProducts(),
    ])
      .then( results => {
        const [orders, allCustomers, allProducts] = results;

        const buyers: {[id: string]: any} = orders.reduce( (pv, order) => {

          const customer: Customer = allCustomers.find( c => c.id === order.customer);
          const key = customer.firstName + ' ' + customer.lastName;
          if (!(key in pv)) {
            pv[key] = { totalPrice: 0 };
          }

          pv[key].totalPrice = order.products.reduce(( total, id ) => {
            const product: Product = allProducts.find( p => p.id === id );
            return total + product.price;
          }, pv[key].totalPrice );

          return pv;
        }, {});

        return Object.keys(buyers).map( key =>  {
          return { customerName: key, totalPrice: buyers[key].totalPrice.toFixed(2) };
        });
      });

  }
}
