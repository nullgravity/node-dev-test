import { Injectable } from '@nestjs/common';
import { Order } from '../Model/Order';
import { Product } from '../Model/Product';
import { Customer } from '../Model/Customer';

/**
 * Data layer - mocked
 */
@Injectable()
export class Repository {
  fetchOrders(): Promise<Order[]> {
    return new Promise(resolve => resolve(require('../Resources/Data/orders')));
  }

  fetchOrdersForDay(date: Date): Promise<Order[]> {
    return this.fetchOrders()
      .then( (orders: Order[]) => {
        return orders.filter( order => {
          const createdAt: Date = new Date(order.createdAt);
          const theSameDay = (createdAt.getTime() - date.getTime()) === 0;
          return theSameDay;
        });
      });
  }

  fetchProducts(): Promise<Product[]> {
    return new Promise(resolve =>
      resolve(require('../Resources/Data/products')),
    );
  }

  fetchCustomers(): Promise<Customer[]> {
    return new Promise(resolve =>
      resolve(require('../Resources/Data/customers')),
    );
  }
}
