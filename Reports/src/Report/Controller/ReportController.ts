import { Controller, Get, Param, Res, HttpStatus } from '@nestjs/common';
import { OrderMapper } from './../../Order/Service/OrderMapper';
import { Response } from 'express';
import { Product } from 'src/Order/Model/Product';
import { IBestBuyers, IBestSellers } from '../Model/IReports';

@Controller()
export class ReportController {

  constructor(private readonly orderMapper: OrderMapper) {}

  aggregateProductsToHaveBestSellers(products: Product[]): IBestSellers[] {
    const bag: { [id: number]: IBestSellers } = {};

    products.forEach(( p: Product ) => {
      if (p.id in bag) {
        bag[p.id].quantity += 1;
        bag[p.id].totalPrice += p.price;
      } else {
        bag[p.id] = { productName: p.name, quantity: 1, totalPrice: p.price };
      }
    });

    return Object.keys(bag).map( key => bag[key] );
  }

  @Get('/report/products/:date')
  bestSellers(@Param('date') date: string, @Res() res: Response) {

    // validate date
    if (!date.match(/^\d{4}-(0[0-9]|1[0-2])-(0[0-9]|1-2[0-9]|3[0-1])$/)) {
      // in case of invalid 'date' param return http 400
      return res.status(HttpStatus.BAD_REQUEST).json({status: false, message: 'bad parameter'});
    }

    const givenDay = new Date(date);
    // get bestsellers products for given day
    this.orderMapper.getBestSellers(givenDay)
      .then(( products: Product[] ) => {
        const bestSellers = this.aggregateProductsToHaveBestSellers(products);
        res.status(HttpStatus.OK).json(bestSellers);
      })
      .catch(( err ) => {
        res.status(500).json({ status: false, error: 'error message' });
      });
  }

  @Get('/report/customer/:date')
  bestBuyers(@Param('date') date: string, @Res() res: Response) {

    if (!date.match(/^\d{4}-(0[0-9]|1[0-2])-(0[0-9]|1-2[0-9]|3[0-1])$/)) {
      // in case of invalid 'date' param return http 400
      return res.status(HttpStatus.BAD_REQUEST).json({status: false, message: 'bad parameter'});
    }

    const givenDay = new Date(date);

    this.orderMapper.getBestBuyers(givenDay)
      .then(( result: IBestBuyers[] ) => {
        result.sort( (a, b) => b.totalPrice - a.totalPrice ); // sort desc, highest amount first
        res.status(HttpStatus.OK).json(result);
      })
      .catch(( err ) => {
        res.status(500).json({ status: false, error: 'error message' });
      });
  }

}
