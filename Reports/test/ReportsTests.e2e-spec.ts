import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { ReportModule } from './../src/Report/ReportModule';

describe('', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [ReportModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(404);
  });

  it('/report/products/2019-08-07abc (GET)', () => {
    return request(app.getHttpServer())
      .get('/report/products/2019-08-07abc')
      .expect(400);
  });

  it('/report/products/2019-08-06 (GET)', () => {
    return request(app.getHttpServer())
      .get('/report/products/2019-08-06')
      .expect('Content-Type', /json/)
      .expect('[]')
      .expect(200);
  });

  it('/report/products/2019-08-07 (GET)', () => {
    return request(app.getHttpServer())
      .get('/report/products/2019-08-07')
      .expect('Content-Type', /json/)
      .expect('[{"productName":"Black sport shoes","quantity":2,"totalPrice":220},\
{"productName":"Cotton t-shirt XL","quantity":1,"totalPrice":25.75}]')
      .expect(200);
  });

  it('/report/customer/2019-08-07abc (GET)', () => {
    return request(app.getHttpServer())
      .get('/report/customer/2019-08-07abc')
      .expect(400);
  });

  it('/report/customer/2019-08-06 (GET)', () => {
    return request(app.getHttpServer())
      .get('/report/customer/2019-08-06')
      .expect('Content-Type', /json/)
      .expect('[]')
      .expect(200);
  });

  it('/report/customer/2019-08-07 (GET)', () => {
    return request(app.getHttpServer())
      .get('/report/customer/2019-08-07')
      .expect('Content-Type', /json/)
      .expect('[{"customerName":"John Doe","totalPrice":"135.75"},{"customerName":"Jane Doe","totalPrice":"110.00"}]')
      .expect(200);
  });

});
