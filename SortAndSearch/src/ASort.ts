// implementation of qsort algorithm
export class ASort {
    public constructor() {}

    public sortArray(A: number[]): number[] {
        // check if array is valid and not empty
        if (!A || (A && !A.length))
            return A
        
        // assign left and right edges
        let left = 0;
        let right = A.length - 1;

        // run qsort algorithm
        this.sort(A, left, right)

        return A;
    }

    private swap(A: number[], i:number, j: number): void {
        // swap two elements in array
        let tmp = A[i]
        A[i] = A[j]
        A[j] = tmp
        
        return;
    }

    private partition(A: number[], left: number, right: number): number {
        let i = left + 1, j = right;

        // get partition spot as a most left
        let p = A[left];

        while (true) {
            while (A[i] <= p && i < right) {
                i += 1;
            }
            while (A[j] >= p && j > left) {
                j -= 1;
            }

            if (i >= j) 
                break;

            // set lower and higher elements on right right position
            this.swap(A, i, j)
        }

        this.swap(A,left,j); // put partitioning item into A[j]
        return j; // A[left..j-1] <= A[j] <= A[j+1..right]
    }

    private sort(A: number[], left: number = 0, right: number = -1): void {

        if (left >= right) 
            return;

        let p = this.partition(A, left, right);

        this.sort(A, left, p - 1);
        this.sort(A, p + 1, right);
    }
}