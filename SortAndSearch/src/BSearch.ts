export class BSearch {

    private static instance: BSearch;

    private _operations: number;

    get operations(): number {
        return this._operations;
    }

    private constructor() {}

    static getInstance() {
        if (!BSearch.instance) {
            BSearch.instance = new BSearch();
        }
        return BSearch.instance;
    }

    public search(A: number[], value: number): number {
        if (!A || (A && A.length == 0))
            return -1;

        return this.searchIndex(A, 0, A.length-1, value);
    }

    private searchIndex(A: number[], left: number, right: number, value: number): number {

        this._operations = 0

        while (left <= right) {
            this._operations += 1;

            let idx = Math.floor((left + right) / 2)
            
            if (A[idx] < value) {
                left = idx + 1
            }
            else if (A[idx] > value) {
                right = idx - 1
            }
            else
                return idx;
        }

        return -1;
    }
}