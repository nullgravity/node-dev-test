// implementation of selection sort algorithm
export class BSort {

    public constructor() {}

    public sortArray(A: number[]): number[] {
        // check if array is valid and not empty
        if (!A || (A && !A.length))
            return A

        this.sort(A);

        return A;
    }

    private swap(A: number[], i:number, j: number): void {
        // swap two elements in array
        let tmp = A[i]
        A[i] = A[j]
        A[j] = tmp
        
        return;
    }

    private sort(A: number[]): void {
        let N = A.length;
        for ( let i = 0; i < N; i++) {
            let min = i;
            for ( let j = i+1; j < N; j++) {
                if (A[min] > A[j]) min = j;

            }
            this.swap(A, i, min);
        }
    }
}