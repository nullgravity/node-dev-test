import { ASort } from './ASort';
import { BSort } from './BSort';
import { BSearch } from './BSearch';

const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
const elementsToFind = [1, 5, 13, 27, 77];

var result;

result = new ASort().sortArray([...unsorted]);
console.log(JSON.stringify(result));

const sorted = new BSort().sortArray([...unsorted]);
console.log(JSON.stringify(result));

result = elementsToFind.map( value => BSearch.getInstance().search(sorted, value ));
console.log(JSON.stringify(result));
