import { BSearch } from './../BSearch';


test('undefined array', () => {
  let result = BSearch.getInstance().search(undefined, 10);
  expect(result).toBe(-1);
});

test('empty array', () => {
  let result = BSearch.getInstance().search([], 10);
  expect(result).toBe(-1);
});

test('main array', () => {
  const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
  let array = [...unsorted].sort( (a,b) => a-b);
  
  const elementsToFind = [1, 5, 13, 27, 77];

  let result = elementsToFind.map( value => {
    return BSearch.getInstance().search(array, value);
  })

  expect(result).toEqual([-1, 3, 6, -1, 12]);
})