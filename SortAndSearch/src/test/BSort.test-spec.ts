import { BSort } from './../BSort';


test('undefined array', () => {
  let bsort = new BSort();
  let unsorted = undefined;
  expect(bsort.sortArray(unsorted)).toBe(undefined);
});

test('empty array', () => {
  let bsort = new BSort();
  let unsorted = [];
  expect(bsort.sortArray(unsorted)).toEqual([]);
});

test('sorted array', () => {
  let bsort = new BSort();
  let unsorted = [1,2,3,4,5,6,7];
  expect(bsort.sortArray(unsorted)).toEqual([1,2,3,4,5,6,7]);
});

test('reversed sorted array', () => {
  let bsort = new BSort();
  let unsorted = [7,6,5,4,3,2,1];
  expect(bsort.sortArray(unsorted)).toEqual([1,2,3,4,5,6,7]);
});

test('big array with random values', () => {
  let bsort = new BSort();
  let N = 10000;

  let unsorted = Array.from({length: N}, () => ~~(Math.random() * N));
  let correct = [...unsorted].sort((a,b) => a-b)

  expect(bsort.sortArray(unsorted)).toEqual(correct);
});

test('main array', () => {
  const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
  const correct = [...unsorted].sort((a,b) => a-b);

  let bsort = new BSort();
  expect(bsort.sortArray(unsorted)).toEqual(correct);
})